package com.objects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.constantes.RequeteConstantes;
import com.enums.Categorie;
import com.enums.Satisfaction;
import com.enums.Statut;
import com.singletonFactory.SingletonFactory;

public class Requete implements RequeteConstantes{

	private String sujet = "";
	private String description = "";
	private File fichier;
	private Integer numero;
	private Utilisateur client;
	private Technicien technicien;
	private Statut statut;
	private Categorie categorie;
	private ArrayList<Commentaire> listeCommentaires;
	private Satisfaction satisfaction;

	public Requete(String sujet, String description, Client client, Categorie categorie)
			throws FileNotFoundException, IOException {
		this.sujet = sujet;
		this.description = description;
		this.client = client;
		numero = BanqueRequetes.getInstance().incrementeNumeroRequete();
		statut = Statut.ouvert;
		this.categorie = categorie;
		listeCommentaires = new ArrayList<Commentaire>();
		satisfaction = Satisfaction.nonDecide;
	}

	public Requete(String sujet, String description, Utilisateur client, Categorie categorie,
			Satisfaction satisfaction)
			throws FileNotFoundException, FileNotFoundException, IOException {
		this.client = client;
		this.sujet = sujet;
		this.description = description;
		this.client = client;
		numero = BanqueRequetes.getInstance().incrementeNumeroRequete();
		statut = Statut.ouvert;
		this.categorie = categorie;
		listeCommentaires = new ArrayList<Commentaire>();
		this.satisfaction = satisfaction;
	}

	public Requete(ArrayList<String> proprieteRequetes, String typeBanque) {
		this.sujet = proprieteRequetes.get(0);
		this.description = proprieteRequetes.get(1);
		this.client = SingletonFactory.getAbstractBanqueUtilisateur(typeBanque)
				.chercherUtilisateurParNomUtil(proprieteRequetes.get(2));
		this.categorie = Categorie.fromString(proprieteRequetes.get(3));
		this.satisfaction = Satisfaction.fromString(proprieteRequetes.get(8));
		this.fichier = new File(proprieteRequetes.get(5));
		if (!proprieteRequetes.get(6).equals("null")) {
			this.technicien = (Technicien) SingletonFactory.getAbstractBanqueUtilisateur(typeBanque)
					.chercherUtilisateurParNomUtil(proprieteRequetes.get(6));
		}
		this.numero = Integer.parseInt(proprieteRequetes.get(7));
		this.statut = Statut.fromString(proprieteRequetes.get(4));
	}

	public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFichier(String path) {
		fichier = new File(path);
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public File getFichier() {
		return fichier;
	}

	public void setStatut(Statut statut) {
		this.statut = statut;
	}

	public void finaliser(Statut statut) {
		this.setStatut(statut);
		this.technicien.addRequetesFinies(this);
	}

	public void addCommentaire(String text, Utilisateur auteur) {
		Commentaire nouveauCommentaire = new Commentaire(text, auteur);
		listeCommentaires.add(nouveauCommentaire);
	}

	// Il n'y a pas de Technicien au départ à moins qu'il la crée lui-meme
	// A un certain point il faut donc désigner un technicien
	public void setTech(Utilisateur technicien) {
		if (technicien != null) {
			if (technicien.getRole().equals(TECHNICIEN_TYPE)) {
				this.technicien = (Technicien) technicien;
				this.technicien.addRequeteAssignee(this);
			}
		}
	}

	public void setFile(File fichier) {
		this.fichier = fichier;
	}

	public Integer getNumero() {
		return numero;
	}

	public Statut getStatut() {
		return statut;
	}

	public ArrayList<Commentaire> getListeCommentaires() {
		return listeCommentaires;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public Technicien getTech() {
		return technicien;
	}

	public Utilisateur getClient() {
		return client;
	}

	public String getDescription() {
		return description;
	}

	public Satisfaction getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(Satisfaction satisfaction) {
		if (this.satisfaction == Satisfaction.nonDecide) {
			this.satisfaction = satisfaction;
		}
	}

	public void setListCommentaire(ArrayList<Commentaire> commentaires) {
		this.listeCommentaires = commentaires;

	}
}