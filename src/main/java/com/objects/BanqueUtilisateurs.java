package com.objects;

import java.io.FileNotFoundException;

import com.singletonFactory.AbstractBanqueUtilisateurs;

public class BanqueUtilisateurs extends AbstractBanqueUtilisateurs {

	private static BanqueUtilisateurs instance = null;

	private BanqueUtilisateurs() throws FileNotFoundException {
		super();
	}

	public static BanqueUtilisateurs getInstance() throws FileNotFoundException {
		if (instance != null) {
			return instance;
		} else {
			instance = new BanqueUtilisateurs();
			instance.loadUtilisateur("users");
			return instance;
		}
	}
}