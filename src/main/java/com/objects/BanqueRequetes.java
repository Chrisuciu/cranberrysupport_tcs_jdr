package com.objects;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.singletonFactory.AbstractBanqueRequetes;

public class BanqueRequetes extends AbstractBanqueRequetes {
	private static BanqueRequetes instance = null;

	private BanqueRequetes() throws FileNotFoundException, IOException {
		super();
	}

	public static BanqueRequetes getInstance() throws FileNotFoundException, IOException {
		if (instance != null) {
			return instance;
		} else {
			instance = new BanqueRequetes();
			instance.loadRequetes("requests", "comments", "reel");
			return instance;
		}
	}
}
