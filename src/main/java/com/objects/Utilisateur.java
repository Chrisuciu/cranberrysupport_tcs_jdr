package com.objects;

import java.util.ArrayList;

import com.enums.Statut;

public abstract class Utilisateur {

	protected String nom;
	protected String prenom;
	protected String nomUtilisateur;
	protected String motDePasse;
	protected boolean isAdministrateur;
	protected boolean isSupprime;
	public String role;
	public ArrayList<String> infos;

	public Utilisateur(String prenom, String nom, String nomUtilisateur, String motDePasse,
			String role, boolean isAdministrateur, boolean isSupprime) {
		this.prenom = prenom;
		this.nom = nom;
		this.nomUtilisateur = nomUtilisateur;
		this.motDePasse = motDePasse;
		this.role = role;
		this.isAdministrateur = isAdministrateur;
		this.isSupprime = isSupprime;
	}

	public boolean isSupprime() {
		return isSupprime;
	}

	public void setSupprime(boolean isSupprime) {
		this.isSupprime = isSupprime;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public boolean isAdministrateur() {
		return isAdministrateur;
	}

	public void setAdministrateur(boolean isAdministrateur) {
		this.isAdministrateur = isAdministrateur;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getRole() {
		return role;
	}

	public ArrayList<String> fetchInfos() {
		this.infos = new ArrayList<>();
		infos.add(nom);
		infos.add(prenom);
		infos.add(nomUtilisateur);
		infos.add(role);
		return infos;
	}

	public String getNom() {
		return nom;
	}

	public boolean verificationConnexion(String nomUtilisateur, String motDePasse) {
		if (this.nomUtilisateur.equalsIgnoreCase(nomUtilisateur)
				&& this.motDePasse.equals(motDePasse)) {
			return true;
		} else {
			return false;
		}
	}

	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public abstract ArrayList<Requete> getListeSelonStatut(Statut statut);

	public abstract ArrayList<Requete> getListeRequetesTotale();

	public abstract void addRequete(Requete requete);

	public abstract ArrayList<Requete> getListePersonnelle();
}