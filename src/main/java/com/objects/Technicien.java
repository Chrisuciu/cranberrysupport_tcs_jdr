package com.objects;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.enums.Statut;
import com.singletonFactory.AbstractBanqueUtilisateurs;
import com.singletonFactory.SingletonFactory;

public class Technicien extends Utilisateur {

	private ArrayList<Requete> listeRequetesTotal;
	private ArrayList<Requete> listeRequetesFinies;
	private ArrayList<Requete> listeRequetesEnCours;
	private ArrayList<Requete> listeRequetesPersonnelle;
	private boolean isAdministrateur;

	private static final String STATUT_ABANDONNEE = "Statut abandonnée: ";
	private static final String STATUT_SUCCES = "Statut succès: ";
	private static final String STATUT_EN_TRAITEMENT = "Statut En traitement: ";
	private static final String STATUT_OUVERT = "Statut ouvert: ";

	public Technicien(String prenom, String nom, String nomUtilisateur, String motDePasse,
			String role, boolean isAdministrateur, boolean isSupprime) {
		super(prenom, nom, nomUtilisateur, motDePasse, role, isAdministrateur, isSupprime);
		listeRequetesTotal = new ArrayList<Requete>();
		listeRequetesEnCours = new ArrayList<Requete>();
		listeRequetesFinies = new ArrayList<Requete>();
		listeRequetesPersonnelle = new ArrayList<Requete>();
		this.isAdministrateur = isAdministrateur;
	}

	public Technicien(ArrayList<String> propriete) {
		super(propriete.get(0), propriete.get(1), propriete.get(2), propriete.get(3),
				propriete.get(4), Boolean.parseBoolean(propriete.get(5)),
				Boolean.parseBoolean(propriete.get(6)));
		listeRequetesTotal = new ArrayList<Requete>();
		listeRequetesEnCours = new ArrayList<Requete>();
		listeRequetesFinies = new ArrayList<Requete>();
		listeRequetesPersonnelle = new ArrayList<Requete>();
	}

	@Override
	public ArrayList<Requete> getListeRequetesTotale() {
		return listeRequetesTotal;
	}

	public void addRequeteAssignee(Requete requete) {
		listeRequetesEnCours.add(requete);
		listeRequetesTotal.add(requete);
	}

	public void addRequetesFinies(Requete requete) {
		listeRequetesFinies.add(requete);
		listeRequetesEnCours.remove(requete);
	}

	@Override
	public void addRequete(Requete requete) {
		listeRequetesTotal.add(requete);
		listeRequetesPersonnelle.add(requete);
	}

	@Override
	public ArrayList<Requete> getListePersonnelle() {
		return listeRequetesPersonnelle;
	}

	@Override
	public ArrayList<Requete> getListeSelonStatut(Statut statut) {
		ArrayList<Requete> listeRequetes = new ArrayList<Requete>();
		for (int i = 0; i < listeRequetesTotal.size(); i++) {
			if (listeRequetesTotal.get(i).getStatut().equals(statut)) {
				listeRequetes.add(listeRequetesTotal.get(i));
			}
		}
		return listeRequetes;
	}

	public ArrayList<Requete> getListeRequetesEnCours() {
		return listeRequetesEnCours;
	}

	public void createUser(Utilisateur utilisateur, String typeBanque) {
		if (this.isAdministrateur) {
			SingletonFactory.getAbstractBanqueUtilisateur(typeBanque).addUtilisateur(utilisateur);
			if (typeBanque.equals("reel")) {
				SingletonFactory.getAbstractBanqueUtilisateur(typeBanque).save("users");
			} else {
				SingletonFactory.getAbstractBanqueUtilisateur(typeBanque).save("fakeUsers");
			}
		}
	}

	public String getRequeteParStatut() {
		String affichageOuvert = STATUT_OUVERT;
		int nbRequeteOuvert = 0;
		String affichageTraitement = STATUT_EN_TRAITEMENT;
		int nbRequeteEnTraitement = 0;
		String affichageSucces = STATUT_SUCCES;
		int nbRequeteSucces = 0;
		String affichageAbandonnee = STATUT_ABANDONNEE;
		int nbRequeteFinalAbandon = 0;
		String affichageResultat = "";
		for (int i = 0; i < listeRequetesTotal.size(); i++) {
			switch (listeRequetesTotal.get(i).getStatut()) {
			case ouvert:
				nbRequeteOuvert++;
				break;
			case enTraitement:
				nbRequeteEnTraitement++;
				break;
			case finalSucces:
				nbRequeteSucces++;
				break;
			case finalAbandon:
				nbRequeteFinalAbandon++;
				break;
			default:
				break;

			}
		}

		affichageResultat = affichageOuvert + nbRequeteOuvert + "\n" + affichageTraitement
				+ nbRequeteEnTraitement + "\n" + affichageSucces + nbRequeteSucces + "\n"
				+ affichageAbandonnee + nbRequeteFinalAbandon + "\n";
		return affichageResultat;
	}

	public void deleteUtilisateur(String nomUtilisateur, String typeBanque)
			throws FileNotFoundException {
		if (isAdministrateur) {
			AbstractBanqueUtilisateurs abstractBanqueUtilisateurs = SingletonFactory
					.getAbstractBanqueUtilisateur(typeBanque);
			abstractBanqueUtilisateurs.chercherUtilisateurParNomUtil(nomUtilisateur)
					.setSupprime(true);
			if (typeBanque.equals("reel")) {
				SingletonFactory.getAbstractBanqueUtilisateur(typeBanque).save("users");
			} else {
				SingletonFactory.getAbstractBanqueUtilisateur(typeBanque).save("fakeUsers");
			}
		}
	}
}