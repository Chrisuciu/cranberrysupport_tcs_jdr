package com.objects;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.constantes.ClientConstantes;
import com.enums.Categorie;
import com.enums.Statut;

public class Client extends Utilisateur implements ClientConstantes{
	
	private ArrayList<Requete> listeRequetesClient;

	public Client(String prenom, String nom, String nomUtilisateur, String motDePasse, String role,
			boolean isAdministrateur, boolean isSupprime) {
		super(prenom, nom, nomUtilisateur, motDePasse, role, isAdministrateur, isSupprime);
		isAdministrateur = false;
		listeRequetesClient = new ArrayList<Requete>();
	}

	public Client(ArrayList<String> propriete) {
		super(propriete.get(0), propriete.get(1), propriete.get(2), propriete.get(3),
				propriete.get(4), Boolean.parseBoolean(propriete.get(5)),
				Boolean.parseBoolean(propriete.get(6)));

		listeRequetesClient = new ArrayList<Requete>();
	}

	@Override
	public void addRequete(Requete nouvelleRequete) {
		listeRequetesClient.add(nouvelleRequete);
	}

	public Requete trouverRequeteParSujet(String sujet) throws FileNotFoundException, IOException {
		for (Requete requeteCourante : listeRequetesClient) {
			if (requeteCourante.getSujet().equals(sujet)) {
				return requeteCourante;
			}
		}
		return new Requete("requeteNonTrouvé", "requeteNonTrouvé", this, Categorie.autre);
	}

	@Override
	public ArrayList<Requete> getListeRequetesTotale() {
		return listeRequetesClient;
	}

	@Override
	public ArrayList<Requete> getListePersonnelle() {
		throw new UnsupportedOperationException(NOT_SUPPORTED_YET_MESSAGE);
	}

	@Override
	public ArrayList<Requete> getListeSelonStatut(Statut statut) {
		throw new UnsupportedOperationException(NOT_SUPPORTED_YET_MESSAGE);
	}
}