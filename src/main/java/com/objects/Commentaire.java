package com.objects;

public class Commentaire {

	protected String commentaire;
	protected Utilisateur auteur;

	public Commentaire(String commentaire, Utilisateur auteur) {
		this.commentaire = commentaire;
		this.auteur = auteur;
	}

	public Utilisateur getAuteur() {
		return auteur;
	}

	public String getComment() {
		return commentaire;
	}

	public String toString() {
		return getAuteur().getNomUtilisateur() + ": " + getComment() + "\n";
	}
}