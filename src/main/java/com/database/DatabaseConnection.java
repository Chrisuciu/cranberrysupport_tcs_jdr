package com.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.constantes.DatabaseConnectionConstantes;
import com.objects.Commentaire;
import com.objects.Requete;
import com.objects.Utilisateur;

public class DatabaseConnection implements DatabaseConnectionConstantes {

	public static DatabaseConnection instance = null;
	private Connection connection = null;

	private DatabaseConnection() {
		try {
			// create a database connection
			connection = DriverManager.getConnection(DATABASE_LOCATION);
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			statement.executeUpdate("create table if not exists users " + USER_PROPS);
			statement.executeUpdate("create table if not exists fakeUsers " + USER_PROPS);
			statement.executeUpdate("create table if not exists requests " + REQUEST_PROPS);
			statement.executeUpdate("create table if not exists fakeRequests " + REQUEST_PROPS);
			statement.executeUpdate("create table if not exists comments " + COMMENTAIRE_PROPS);
			statement.executeUpdate("create table if not exists fakeComments " + COMMENTAIRE_PROPS);
		} catch (SQLException e) {
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
	}

	public static DatabaseConnection getInstance() {
		if (instance == null) {
			instance = new DatabaseConnection();
		}
		return instance;
	}

	public void populateUsers(String tableName) {
		Statement statement;
		try {
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate("insert into " + tableName + " values" + BASE_USER_VALUES);

		} catch (SQLException e) {
			e.printStackTrace();
		} // set timeout to 30 sec.
	}

	public ResultSet select(String tableName) {
		Statement statement;
		try {
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("select * from " + tableName);
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		} // set timeout to 30 sec.
		return null;
	}

	public void dropTable(String tableName) {
		Statement statement;
		try {
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate("drop table if exists " + tableName);
		} catch (SQLException e) {
			e.printStackTrace();
		} // set timeout to 30 sec.
	}

	public void writeUsers(String tableName, ArrayList<Utilisateur> listeUtilisateurs) {
		if (listeUtilisateurs.size() != 0) {
			Statement statement;
			String thingsToInsert = "";
			for (Utilisateur utilisateur : listeUtilisateurs) {
				thingsToInsert += "(";
				thingsToInsert += "'" + utilisateur.getPrenom() + "', ";
				thingsToInsert += "'" + utilisateur.getNom() + "', ";
				thingsToInsert += "'" + utilisateur.getNomUtilisateur() + "', ";
				thingsToInsert += "'" + utilisateur.getMotDePasse() + "', ";
				thingsToInsert += "'" + utilisateur.getRole() + "', ";
				thingsToInsert += "'" + utilisateur.isAdministrateur() + "', ";
				thingsToInsert += "'" + utilisateur.isSupprime() + "'";
				thingsToInsert += "), ";
			}
			thingsToInsert = thingsToInsert.substring(0, thingsToInsert.length() - 2);
			try {
				statement = connection.createStatement();
				statement.setQueryTimeout(30);
				statement.executeUpdate("insert into " + tableName + " values" + thingsToInsert);
			} catch (SQLException e) {
				e.printStackTrace();
			} // set timeout to 30 sec.
		}
	}

	public void writeRequest(String tableName, String commentTable,
			ArrayList<Requete> listeRequetes) {
		if (listeRequetes.size() != 0) {
			Statement statement;
			String thingsToInsert = "";
			for (Requete requete : listeRequetes) {
				thingsToInsert += "(";
				thingsToInsert += "'" + requete.getSujet() + "', ";
				thingsToInsert += "'" + requete.getDescription() + "', ";
				thingsToInsert += "'" + requete.getClient().getNomUtilisateur() + "', ";
				thingsToInsert += "'" + requete.getCategorie().getValeurString() + "', ";
				thingsToInsert += "'" + requete.getStatut().getValeurString() + "', ";
				if (requete.getFichier() == null) {
					thingsToInsert += "'null', ";
				} else {
					thingsToInsert += "'" + requete.getFichier().getPath() + "', ";
				}
				if (requete.getTech() == null) {
					thingsToInsert += "'null', ";
				} else {
					thingsToInsert += "'" + requete.getTech().getNomUtilisateur() + "', ";
				}
				thingsToInsert += "'" + requete.getNumero() + "', ";
				thingsToInsert += "'" + requete.getSatisfaction() + "'";
				thingsToInsert += "), ";
				writeComments(requete.getListeCommentaires(), requete.getNumero(), commentTable);
			}
			thingsToInsert = thingsToInsert.substring(0, thingsToInsert.length() - 2);
			try {
				statement = connection.createStatement();
				statement.setQueryTimeout(30);
				statement.executeUpdate("insert into " + tableName + " values" + thingsToInsert);
			} catch (SQLException e) {
				e.printStackTrace();
			} // set timeout to 30 sec.
		}
	}

	private void writeComments(ArrayList<Commentaire> listeCommentaires, int commentId,
			String tableName) {
		if (listeCommentaires.size() != 0) {
			Statement statement;
			String thingsToInsert = "";
			try {
				statement = connection.createStatement();
				statement.setQueryTimeout(30);
				for (Commentaire comment : listeCommentaires) {
					thingsToInsert += "(";
					thingsToInsert += "'" + commentId + "', ";
					thingsToInsert += "'" + comment.getAuteur().getNomUtilisateur() + "', ";
					thingsToInsert += "'" + comment.getComment() + "'";
					thingsToInsert += "), ";
				}
				thingsToInsert = thingsToInsert.substring(0, thingsToInsert.length() - 2);
				statement.executeUpdate("insert into " + tableName + " values" + thingsToInsert);
			} catch (SQLException e) {
				e.printStackTrace();
			} // set timeout to 30 sec.
		}
	}

	public void createUserTable(String tableName, String props) {
		Statement statement;
		try {
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate("create table if not exists " + tableName + " " + props);
		} catch (SQLException e) {
			e.printStackTrace();
		} // set timeout to 30 sec.
	}

	public void createRequestTables(String requestTableName) {
		Statement statement;
		try {
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate(
					"create table if not exists " + requestTableName + " " + REQUEST_PROPS);
		} catch (SQLException e) {
			e.printStackTrace();
		} // set timeout to 30 sec.
	}

	public void createCommentTables(String commentTableName) {
		Statement statement;
		try {
			statement = connection.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate(
					"create table if not exists " + commentTableName + " " + COMMENTAIRE_PROPS);
		} catch (SQLException e) {
			e.printStackTrace();
		} // set timeout to 30 sec.
	}
}
