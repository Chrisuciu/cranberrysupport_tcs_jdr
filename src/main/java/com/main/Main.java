package com.main;

import java.io.FileNotFoundException;

import java.io.IOException;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.frame.CranberryFrame;
import com.objects.BanqueRequetes;
import com.singletonFactory.SingletonFactory;

public class Main {

	public static void main(String args[]) throws FileNotFoundException, IOException {

		BanqueRequetes.getInstance();

		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(CranberryFrame.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(CranberryFrame.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(CranberryFrame.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(CranberryFrame.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		}
		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new CranberryFrame().setVisible(true);
			}
		});
		// S'assure de sauvegarder avant de quitter l'application
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					SingletonFactory.getAbstractBanqueUtilisateur("reel").save("users");
					SingletonFactory.getAbstractBanqueRequetes("reel").saveRequetes("requests",
							"comments");
				} catch (IOException ex) {
					Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}));
	}
}
