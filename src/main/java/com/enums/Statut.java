package com.enums;

public enum Statut {
	ouvert("ouvert"), enTraitement("en traitement"), finalAbandon("Abandon"), finalSucces("Succès");
	String valeur;

	Statut(String s) {
		this.valeur = s;
	}

	public String getValeurString() {
		return valeur;
	}

	public static Statut fromString(String text) {
		if (text != null) {
			if (Statut.ouvert.getValeurString().equalsIgnoreCase(text)) {
				return Statut.ouvert;
			} else if (Statut.enTraitement.getValeurString().equalsIgnoreCase(text)) {
				return Statut.enTraitement;
			} else if (Statut.finalAbandon.getValeurString().equalsIgnoreCase(text)) {
				return Statut.finalAbandon;
			} else if (Statut.finalSucces.getValeurString().equalsIgnoreCase(text)) {
				return Statut.finalSucces;
			}
		}
		return Statut.ouvert;
	}
}
