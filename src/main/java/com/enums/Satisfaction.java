package com.enums;

public enum Satisfaction {
	satisfait("Utilisateur satisfait"), insatisfait("Utilisateur insatisfait"), nonDecide(
			"Utilisateur n'a pas décicé");
	String valeur;

	Satisfaction(String valeur) {
		this.valeur = valeur;
	}

	public String getValeurString() {
		return valeur;
	}

	public static Satisfaction fromString(String text) {
		if (text != null) {
			if (text.equals("satisfait")) {
				return Satisfaction.satisfait;
			} else if (text.equals("insatisfait")) {
				return Satisfaction.insatisfait;
			} else if (text.equals("nonDecide")) {
				return Satisfaction.nonDecide;
			}
		}
		return Satisfaction.nonDecide;
	}
}
