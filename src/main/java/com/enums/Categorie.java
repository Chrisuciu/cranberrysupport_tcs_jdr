package com.enums;

public enum Categorie {
	posteDeTravail("Poste de travail"), serveur("Serveur"), serviceWeb("Service web"), compteUsager(
			"Compte usager"), autre("Autre");
	String value;

	Categorie(String value) {
		this.value = value;
	}

	public String getValeurString() {
		return value;
	}

	public static Categorie fromString(String text) {
		if (text != null) {
			if (Categorie.posteDeTravail.getValeurString().equalsIgnoreCase(text)) {
				return Categorie.posteDeTravail;
			} else if (Categorie.serveur.getValeurString().equalsIgnoreCase(text)) {
				return Categorie.serveur;
			} else if (Categorie.serviceWeb.getValeurString().equalsIgnoreCase(text)) {
				return Categorie.serviceWeb;
			} else if (Categorie.compteUsager.getValeurString().equalsIgnoreCase(text)) {
				return Categorie.compteUsager;
			} else if (Categorie.autre.getValeurString().equalsIgnoreCase(text)) {
				return Categorie.autre;
			}
		}
		return Categorie.autre;
	}
}
