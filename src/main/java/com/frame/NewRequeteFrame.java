package com.frame;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

import com.enums.Categorie;
import com.enums.Satisfaction;
import com.objects.BanqueRequetes;
import com.objects.Requete;
import com.objects.Utilisateur;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public class NewRequeteFrame extends javax.swing.JFrame {
	Utilisateur clientConnecter;
	JFrame jframePrecedent;
	String path;
	File fichier;

	public NewRequeteFrame(Utilisateur clientConnecter, JFrame pagePrecedente) {
		initComponents();
		this.setVisible(true);
		this.clientConnecter = clientConnecter;
		jframePrecedent = pagePrecedente;
	}

	private void initComponents() {
		jChoixFichier = new javax.swing.JFileChooser();
		titreLabel = new javax.swing.JLabel();
		sujetLabel = new javax.swing.JLabel();
		sujetJTF = new javax.swing.JTextField();
		descriptionLabel = new javax.swing.JLabel();
		descpriptionScroll = new javax.swing.JScrollPane();
		descriptionArea = new javax.swing.JTextArea();
		fichierLabel = new javax.swing.JLabel();
		pathJTF = new javax.swing.JTextField();
		choixPathBoutton = new javax.swing.JButton();
		categorieBox = new javax.swing.JComboBox();
		categorieLabel = new javax.swing.JLabel();
		terminerBoutton = new javax.swing.JButton();
		revenirBoutton = new javax.swing.JButton();
		jSeparator1 = new javax.swing.JSeparator();

		jChoixFichier.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusGained(java.awt.event.FocusEvent evt) {
				fileChooserFocusGained(evt);
			}
		});
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		titreLabel.setText("Nouvelle requête");

		sujetLabel.setText("Sujet de la requête:");

		sujetJTF.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sujetFldActionPerformed(evt);
			}
		});
		descriptionLabel.setText("Description:");

		descriptionArea.setColumns(20);
		descriptionArea.setRows(5);
		descriptionArea.setBorder(null);
		descpriptionScroll.setViewportView(descriptionArea);

		fichierLabel.setText("Fichier:");

		pathJTF.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				pathFldActionPerformed(evt);
			}
		});
		choixPathBoutton.setText("Téléverser un fichier");
		choixPathBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selPathBtnActionPerformed(evt);
			}
		});
		categorieBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
				"Poste de travail", "Serveur", "Service web", "Compte usager", "Autre" }));
		categorieBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				catBoxActionPerformed(evt);
			}
		});
		categorieLabel.setText("Catégorie:");

		terminerBoutton.setText("Terminer");
		terminerBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				doneBtnActionPerformed(evt);
			}
		});
		revenirBoutton.setText("Revenir");
		revenirBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quitBtnActionPerformed(evt);
			}
		});
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(titreLabel)
								.addGroup(layout.createSequentialGroup().addGroup(layout
										.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(sujetLabel).addComponent(descriptionLabel)
										.addComponent(fichierLabel))
										.addGap(18, 18, 18)
										.addGroup(layout
												.createParallelGroup(
														javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(pathJTF,
														javax.swing.GroupLayout.DEFAULT_SIZE, 246,
														Short.MAX_VALUE)
												.addComponent(sujetJTF,
														javax.swing.GroupLayout.DEFAULT_SIZE, 246,
														Short.MAX_VALUE)
												.addComponent(descpriptionScroll,
														javax.swing.GroupLayout.DEFAULT_SIZE, 246,
														Short.MAX_VALUE)
												.addComponent(choixPathBoutton))))
								.addContainerGap(30, Short.MAX_VALUE))
						.addGroup(layout.createSequentialGroup().addComponent(categorieLabel)
								.addGap(63, 63, 63)
								.addComponent(categorieBox, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(176, Short.MAX_VALUE))
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
								layout.createSequentialGroup().addGap(124, 124, 124)
										.addComponent(terminerBoutton).addGap(18, 18, 18)
										.addComponent(revenirBoutton)
										.addContainerGap(104, Short.MAX_VALUE))))
				.addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 400,
						Short.MAX_VALUE));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(titreLabel)
						.addGap(18, 18, 18)
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(sujetLabel).addComponent(sujetJTF,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(descriptionLabel).addComponent(descpriptionScroll,
										javax.swing.GroupLayout.PREFERRED_SIZE, 105,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(pathJTF, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(fichierLabel))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(choixPathBoutton).addGap(18, 18, 18)
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(categorieBox, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(categorieLabel))
						.addGap(27, 27, 27)
						.addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(revenirBoutton).addComponent(terminerBoutton))
						.addContainerGap(31, Short.MAX_VALUE)));
		pack();
	}

	private void sujetFldActionPerformed(java.awt.event.ActionEvent evt) {
		// a supprimer
	}

	// affiche le path du fichier
	private void pathFldActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_pathFldActionPerformed
		pathJTF.setText(jChoixFichier.getSelectedFile().getPath());
	}// GEN-LAST:event_pathFldActionPerformed

	// FileShooser: on crée un nouveau fichier dans le /dat pour le conserver
	// peut importe d'où il vient pour que le technicien puisse l'ouvrir
	private void selPathBtnActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_selPathBtnActionPerformed
		jChoixFichier.showOpenDialog(jframePrecedent);
		pathFldActionPerformed(evt);
		try {
			fichier = new File("dat/" + jChoixFichier.getSelectedFile().getName());
			InputStream srcFile = new FileInputStream(jChoixFichier.getSelectedFile());
			OutputStream newFile = new FileOutputStream(fichier);
			byte[] buf = new byte[4096];
			int len;
			while ((len = srcFile.read(buf)) > 0) {
				newFile.write(buf, 0, len);
			}
			srcFile.close();
			newFile.close();
		} catch (IOException ex) {
			Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void catBoxActionPerformed(java.awt.event.ActionEvent evt) {
	}

	// Crée la requête avec toutes les informations entrée dans les cases
	// et les objets sélectionnés dans les menus
	private void doneBtnActionPerformed(java.awt.event.ActionEvent evt) {
		try {
			Requete requete = new Requete(sujetJTF.getText(),
					descriptionArea.getText().replaceAll("\n", " "), clientConnecter,
					Categorie.fromString((String) categorieBox.getSelectedItem()),
					Satisfaction.nonDecide);
			BanqueRequetes.getInstance().newRequete(requete);
			Requete tempo = BanqueRequetes.getInstance().getDerniereRequete();
			BanqueRequetes.getInstance().getDerniereRequete().setFile(fichier);

			this.setVisible(false);
			if (clientConnecter.getRole().equals("client")) {
				ClientLoggedFrame retourPageClient = new ClientLoggedFrame(clientConnecter);
				retourPageClient.setVisible(true);
			} else {
				TechLoggedFrame retourPageTech = new TechLoggedFrame(clientConnecter);
				retourPageTech.setVisible(true);
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// Quitter: revenir à la fenêtre précédante
	private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
		jframePrecedent.setVisible(true);
	}

	private void fileChooserFocusGained(java.awt.event.FocusEvent evt) {
		// a supprimer
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JComboBox categorieBox;
	private javax.swing.JLabel categorieLabel;
	private javax.swing.JTextArea descriptionArea;
	private javax.swing.JScrollPane descpriptionScroll;
	private javax.swing.JLabel descriptionLabel;
	private javax.swing.JButton terminerBoutton;
	private javax.swing.JLabel fichierLabel;
	private javax.swing.JFileChooser jChoixFichier;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JTextField pathJTF;
	private javax.swing.JButton revenirBoutton;
	private javax.swing.JButton choixPathBoutton;
	private javax.swing.JTextField sujetJTF;
	private javax.swing.JLabel sujetLabel;
	private javax.swing.JLabel titreLabel;
	// End of variables declaration//GEN-END:variables
}
