package com.frame;

public class CranberryFrame extends javax.swing.JFrame {

	/** Creates new form CranberryFrame */
	public CranberryFrame() {
		initComponents();
		this.setVisible(true);
	}

	private void initComponents() {
		jFrame1 = new javax.swing.JFrame();
		demarrageLbl = new javax.swing.JLabel();
		technicienBoutton = new javax.swing.JButton();
		clientBoutton = new javax.swing.JButton();

		javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(
				jFrame1.getContentPane());
		jFrame1.getContentPane().setLayout(jFrame1Layout);
		jFrame1Layout.setHorizontalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGap(0, 400, Short.MAX_VALUE));
		jFrame1Layout.setVerticalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGap(0, 300, Short.MAX_VALUE));
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setBackground(new java.awt.Color(255, 255, 255));
		setName("demarrage");
		demarrageLbl.setText("Démarrer l'application en tant que:");
		technicienBoutton.setText("Technicien");
		technicienBoutton.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				techBtnMouseClicked(evt);
			}
		});
		technicienBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				techBtnActionPerformed(evt);
			}
		});
		clientBoutton.setText("Utilisateur");
		clientBoutton.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				clientBtnMouseClicked(evt);
			}
		});
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(32, 32, 32)
						.addComponent(demarrageLbl).addGap(35, 35, 35).addComponent(clientBoutton)
						.addGap(18, 18, 18).addComponent(technicienBoutton)
						.addContainerGap(42, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(demarrageLbl).addComponent(clientBoutton)
								.addComponent(technicienBoutton))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		pack();
	}

	// Va au login du technicien
	private void techBtnMouseClicked(java.awt.event.MouseEvent evt) {
		this.setVisible(false);
		TechLoginFrame technic = new TechLoginFrame();
	}

	// Va au Login du client
	private void clientBtnMouseClicked(java.awt.event.MouseEvent evt) {
		this.setVisible(false);
		ClientFrame visuelClient = new ClientFrame();
	}

	private void techBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// à détruire
	}// GEN-LAST:event_techBtnActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton clientBoutton;
	private javax.swing.JLabel demarrageLbl;
	private javax.swing.JFrame jFrame1;
	private javax.swing.JButton technicienBoutton;
	// End of variables declaration//GEN-END:variables
}
