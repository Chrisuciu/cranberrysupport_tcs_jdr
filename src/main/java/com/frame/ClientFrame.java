package com.frame;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.objects.BanqueUtilisateurs;
import com.objects.Utilisateur;

@SuppressWarnings("serial")
public class ClientFrame extends javax.swing.JFrame {

	/** Creates new form RaspberryFrame */
	public ClientFrame() {
		initComponents();
		setVisible(true);
	}

	private String nomUtilisateur;
	private String motDePasse;
	private Utilisateur utilisateurPotentielle;

	private void initComponents() {

		nomJlabel = new javax.swing.JLabel();
		motDePasseJLabel = new javax.swing.JLabel();
		nomExempleJLabel = new javax.swing.JLabel();
		motDePasseExempleJLabel = new javax.swing.JLabel();
		utilisateurJLabel = new javax.swing.JTextField();
		motDePasseJTF = new javax.swing.JTextField();
		okButton = new javax.swing.JButton();
		annulerButton = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		nomJlabel.setText("Nom d'utilisateur:");

		motDePasseJLabel.setText("Mot de passe:");

		nomExempleJLabel.setText("ex.: Isabeau Desrochers");

		motDePasseExempleJLabel.setText("ex.: MonMdp");

		okButton.setText("OK");
		okButton.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent event) {
				okBtnMouseClicked(event);
			}
		});

		annulerButton.setText("Annuler");
		annulerButton.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent event) {
				annulerBtnMouseClicked(event);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addGroup(layout.createSequentialGroup().addContainerGap()
										.addComponent(okButton,
												javax.swing.GroupLayout.PREFERRED_SIZE, 69,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(annulerButton))
								.addGroup(layout.createSequentialGroup().addGap(37, 37, 37)
										.addGroup(layout
												.createParallelGroup(
														javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(nomJlabel)
												.addComponent(motDePasseJLabel))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(layout
												.createParallelGroup(
														javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(motDePasseJTF,
														javax.swing.GroupLayout.DEFAULT_SIZE, 149,
														Short.MAX_VALUE)
												.addComponent(utilisateurJLabel,
														javax.swing.GroupLayout.DEFAULT_SIZE, 149,
														Short.MAX_VALUE))))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(layout
										.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(motDePasseExempleJLabel)
										.addComponent(nomExempleJLabel))
								.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(22, 22, 22).addGroup(layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(nomJlabel)
						.addComponent(utilisateurJLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addComponent(nomExempleJLabel))
						.addGap(18, 18, 18)
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(motDePasseJLabel)
								.addComponent(motDePasseJTF, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(motDePasseExempleJLabel))
						.addGap(18, 18, 18)
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(annulerButton).addComponent(okButton))
						.addContainerGap(20, Short.MAX_VALUE)));
		pack();
	}// </editor-fold>//GEN-END:initComponents

	// Quand on clique sur OK, on vérifie la validité des informations
	private void okBtnMouseClicked(java.awt.event.MouseEvent event) {// GEN-FIRST:event_okBtnMouseClicked
		try {
			nomUtilisateur = utilisateurJLabel.getText();
			motDePasse = motDePasseJTF.getText();

			utilisateurPotentielle = BanqueUtilisateurs.getInstance()
					.chercherUtilisateurParNomUtil(nomUtilisateur);
			if (utilisateurPotentielle == null) {
				ErrorLoginFrame mauvaisLogin = new ErrorLoginFrame();
				this.setVisible(false);
			}

			if (utilisateurPotentielle.verificationConnexion(nomUtilisateur, motDePasse)) {
				this.setVisible(false);
				ClientLoggedFrame fenetreClient = new ClientLoggedFrame(utilisateurPotentielle);
			} else {
				ErrorLoginFrame mauvaisLogin = new ErrorLoginFrame();
				this.setVisible(false);
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(ClientFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// Bouton annuler: revenir sur nos pas
	private void annulerBtnMouseClicked(java.awt.event.MouseEvent event) {// GEN-FIRST:event_annulerBtnMouseClicked
		this.setVisible(false);
		CranberryFrame revenir = new CranberryFrame();
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton annulerButton;
	private javax.swing.JLabel motDePasseExempleJLabel;
	private javax.swing.JLabel nomExempleJLabel;
	private javax.swing.JTextField motDePasseJTF;
	private javax.swing.JLabel motDePasseJLabel;
	private javax.swing.JLabel nomJlabel;
	private javax.swing.JButton okButton;
	private javax.swing.JTextField utilisateurJLabel;
	// End of variables declaration//GEN-END:variables

}
