package com.frame;

public class RapportFrame extends javax.swing.JFrame {

	/** Creates new form rapportFrame */
	public RapportFrame(String rapport) {
		initComponents();
		rapportArea.setText(rapport);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // on ne veut pas fermer
														// l'app
	}

	private void initComponents() {
		jScrollPane1 = new javax.swing.JScrollPane();
		rapportArea = new javax.swing.JTextArea();
		revenirBoutton = new javax.swing.JButton();
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		rapportArea.setColumns(20);
		rapportArea.setEditable(false);
		rapportArea.setLineWrap(true);
		rapportArea.setRows(5);
		jScrollPane1.setViewportView(rapportArea);
		revenirBoutton.setText("Revenir");
		revenirBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGroup(layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(18, 18, 18).addComponent(
								jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 214,
								javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup().addGap(89, 89, 89)
								.addComponent(revenirBoutton)))
						.addContainerGap(22, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 457,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(revenirBoutton).addContainerGap(18, Short.MAX_VALUE)));
		pack();
	}

	// Reviens à la fenêtre du technicien
	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
	}// GEN-LAST:event_jButton1ActionPerformed
		// Variables declaration - do not modify//GEN-BEGIN:variables

	private javax.swing.JButton revenirBoutton;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextArea rapportArea;
}
