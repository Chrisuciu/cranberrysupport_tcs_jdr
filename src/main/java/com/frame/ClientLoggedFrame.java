package com.frame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;

import com.enums.Satisfaction;
import com.enums.Statut;
import com.objects.BanqueRequetes;
import com.objects.Commentaire;
import com.objects.Requete;
import com.objects.Utilisateur;

public class ClientLoggedFrame extends javax.swing.JFrame {

	Utilisateur utilisateurConnecter;
	String nomClient;
	ArrayList<Requete> listeRequetes;
	Requete requeteAfficher;
	JFrame jframePrecedent;
	private File fichier;
	private boolean afficherCommentaires = false;

	/** Creates new form ClientLoggedFrame */
	public ClientLoggedFrame(Utilisateur clientConnecter) {
		initComponents();
		this.setVisible(true);
		utilisateurConnecter = clientConnecter;
		listeRequetes = clientConnecter.getListeRequetesTotale();

		// Le code qui suit initialise la liste de requête du client
		DefaultListModel listeRequeteAfficher = new DefaultListModel();

		if (clientConnecter.getListeRequetesTotale().isEmpty()) {
			listeRequeteAfficher.addElement("Vous n'avez pas de requête encore.");
		} else {
			for (int i = 0; i < clientConnecter.getListeRequetesTotale().size(); i++) {
				listeRequeteAfficher
						.addElement(clientConnecter.getListeRequetesTotale().get(i).getSujet());

			}
		}
		listRequete.setModel(listeRequeteAfficher); // peint la liste
		commentArea.setVisible(false);
	}

	private void initComponents() {

		choiseDeFichier = new javax.swing.JFileChooser();
		jScrollPane1 = new javax.swing.JScrollPane();
		listRequete = new javax.swing.JList();
		requetesJLabel = new javax.swing.JLabel();
		boutonAjoutRequete = new javax.swing.JButton();
		quitterBoutton = new javax.swing.JButton();
		modificationBoutton = new javax.swing.JButton();
		satisfaitBoutton = new javax.swing.JButton();
		insatisfaitBoutton = new javax.swing.JButton();

		fichierBoutton = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();
		jScrollPane2 = new javax.swing.JScrollPane();
		requeteArea = new javax.swing.JTextArea();
		voirCommentaireBoutton = new javax.swing.JButton();
		jScrollPane3 = new javax.swing.JScrollPane();
		commentArea = new javax.swing.JTextArea();
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		listRequete.setModel(new javax.swing.AbstractListModel() {
			String[] strings = { "bouba" };

			public int getSize() {
				return strings.length;
			}

			public Object getElementAt(int i) {
				return strings[i];
			}
		});
		listRequete.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				listRequeteListValueChanged(evt);
			}
		});
		jScrollPane1.setViewportView(listRequete);

		requetesJLabel.setText("Vos requêtes:");

		boutonAjoutRequete.setText("Faire une nouvelle requête");
		boutonAjoutRequete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addRequeteBtnActionPerformed(evt);
			}
		});

		quitterBoutton.setText("Quitter");
		quitterBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quitBtnActionPerformed(evt);
			}
		});

		modificationBoutton.setText("Ajouter un commentaire");
		modificationBoutton.setEnabled(false);
		modificationBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				modifBtnActionPerformed(evt);
			}
		});
		satisfaitBoutton.setText("Je suis satisfait");
		satisfaitBoutton.setEnabled(false);
		satisfaitBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				requeteAfficher.setSatisfaction(Satisfaction.satisfait);
				satisfaitBoutton.setEnabled(false);
				insatisfaitBoutton.setEnabled(false);
			}
		});
		insatisfaitBoutton.setText("Je ne suis pas satisfait");
		insatisfaitBoutton.setEnabled(false);
		insatisfaitBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				requeteAfficher.setSatisfaction(Satisfaction.insatisfait);
				satisfaitBoutton.setEnabled(false);
				insatisfaitBoutton.setEnabled(false);
			}
		});
		fichierBoutton.setText("Ajouter un fichier");
		fichierBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fichierBtnActionPerformed(evt);
			}
		});
		jLabel1.setText("Modifier la requête sélectionnée:");
		requeteArea.setColumns(20);
		requeteArea.setEditable(false);
		requeteArea.setLineWrap(true);
		requeteArea.setRows(5);
		jScrollPane2.setViewportView(requeteArea);

		voirCommentaireBoutton.setBackground(new java.awt.Color(102, 153, 255));
		voirCommentaireBoutton.setText("Voir les commentaires");
		voirCommentaireBoutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				voirComsBtnActionPerformed(evt);
			}
		});
		commentArea.setColumns(20);
		commentArea.setLineWrap(true);
		commentArea.setRows(5);
		commentArea.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				commentAreaKeyPressed(evt);
			}
		});
		jScrollPane3.setViewportView(commentArea);
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(boutonAjoutRequete).addComponent(requetesJLabel)
						.addGroup(layout.createSequentialGroup().addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
										214, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(quitterBoutton))
								.addGap(10, 10, 10)
								.addGroup(layout
										.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(voirCommentaireBoutton)
										.addComponent(fichierBoutton)
										.addComponent(modificationBoutton)
										.addComponent(satisfaitBoutton)
										.addComponent(insatisfaitBoutton).addComponent(jLabel1)
										.addComponent(jScrollPane3,
												javax.swing.GroupLayout.PREFERRED_SIZE, 166,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(9, 9, 9).addComponent(jScrollPane2,
										javax.swing.GroupLayout.DEFAULT_SIZE, 270,
										Short.MAX_VALUE)))
						.addContainerGap()));
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout
								.createSequentialGroup().addGroup(layout.createParallelGroup(
										javax.swing.GroupLayout.Alignment.TRAILING).addGroup(
												javax.swing.GroupLayout.Alignment.LEADING, layout
														.createSequentialGroup().addGap(76, 76, 76)
														.addComponent(jScrollPane2,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																324, Short.MAX_VALUE))
										.addGroup(layout.createSequentialGroup().addGap(22, 22, 22)
												.addComponent(boutonAjoutRequete).addPreferredGap(
														javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(requetesJLabel).addPreferredGap(
														javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addGroup(layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(layout.createSequentialGroup()
																.addComponent(jScrollPane1,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		290, Short.MAX_VALUE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																.addComponent(quitterBoutton))
														.addGroup(layout.createSequentialGroup()
																.addComponent(
																		voirCommentaireBoutton)
																.addGap(26, 26, 26)
																.addComponent(jLabel1)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(fichierBoutton)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(modificationBoutton)
																.addComponent(satisfaitBoutton)
																.addComponent(insatisfaitBoutton)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(jScrollPane3,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		197, Short.MAX_VALUE)))))
								.addContainerGap()));
		pack();
	}

	// Bouton Ajouter une requête
	private void addRequeteBtnActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
		NewRequeteFrame nouvelleRequete = new NewRequeteFrame(utilisateurConnecter, this);
	}// GEN-LAST:event_addRequeteBtnActionPerformed

	// Lorsqu'on sélectionne un élément de la liste
	// la région à coté se met a jour avec les informations de la requete
	// sélectionnée
	private void listRequeteListValueChanged(javax.swing.event.ListSelectionEvent evt) {
		updateRequete();
	}

	// Bouton quitter: enregistre et quitte
	private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {
		try {
			BanqueRequetes.getInstance().saveRequetes("requests", "comments");
			System.exit(0);
		} catch (IOException ex) {
			Logger.getLogger(ClientLoggedFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// l'ajout d'un fichier passe pas la sélection à l'aide d'un JFileChooser
	private void fichierBtnActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_fichierBtnActionPerformed
		choiseDeFichier.showOpenDialog(jframePrecedent);
		fichier = choiseDeFichier.getSelectedFile();
		try {
			listeRequetes.get(listRequete.getSelectedIndex()).setFile(fichier);
		} catch (IndexOutOfBoundsException e) {
		}

		File fichierChoisis = choiseDeFichier.getSelectedFile();
		if (fichierChoisis != null) {
			try {
				FileWriter newFile = new FileWriter("dat/" + fichierChoisis.getName());
			} catch (IOException ex) {
				Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
			}
			if (fichier.exists()) {
				FichierAddOkFrame ok = new FichierAddOkFrame();
				ok.setVisible(true);
			}
		}
	}

	private void modifBtnActionPerformed(java.awt.event.ActionEvent evt) {
		commentArea.setVisible(true);
		commentArea.requestFocus();
	}

	// Affiche les commentaires
	private void voirComsBtnActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_voirComsBtnActionPerformed
		if (requeteAfficher != null) {
			if (!afficherCommentaires) {
				String s = "";
				for (Commentaire c : requeteAfficher.getListeCommentaires()) {
					s += c.toString();
				}
				requeteArea.setText(s);
				afficherCommentaires = true;
				voirCommentaireBoutton.setText("Voir la rêquete");
				fichierBoutton.setEnabled(false);
				modificationBoutton.setEnabled(true);
			} else {
				updateRequete();
			}
		}
	}

	// Mise a jour de la région d'affichage de la requête
	private void updateRequete() {
		requeteAfficher = listeRequetes.get(listRequete.getSelectedIndex());
		requeteArea.setText("Sujet: " + requeteAfficher.getSujet() + "\nDescription: "
				+ requeteAfficher.getDescription() + "\nCatégorie: "
				+ requeteAfficher.getCategorie().toString() + "\nStatut: "
				+ requeteAfficher.getStatut().toString());
		afficherCommentaires = false;
		voirCommentaireBoutton.setText("Voir les commentaires");
		fichierBoutton.setEnabled(true);
		modificationBoutton.setEnabled(false);
		satisfaitBoutton.setEnabled(false);
		insatisfaitBoutton.setEnabled(false);
		if (requeteAfficher.getSatisfaction() == Satisfaction.nonDecide
				&& (requeteAfficher.getStatut() == Statut.finalSucces
						|| requeteAfficher.getStatut() == Statut.finalAbandon)) {
			satisfaitBoutton.setEnabled(true);
			insatisfaitBoutton.setEnabled(true);
		}
	}

	// Ajoute un commentaire qui a été entré dans la ligne
	// réagit à ENTER
	private void commentAreaKeyPressed(java.awt.event.KeyEvent evt) {
		if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
			requeteAfficher.addCommentaire(commentArea.getText(), utilisateurConnecter);
			String s = "";
			for (Commentaire c : requeteAfficher.getListeCommentaires()) {
				s += c.toString();
			}
			requeteArea.setText(s);
			commentArea.setVisible(false);
			modificationBoutton.requestFocus();
		}
	}// GEN-LAST:event_commentAreaKeyPressed
		// Variables declaration - do not modify//GEN-BEGIN:variables

	private javax.swing.JButton boutonAjoutRequete;
	private javax.swing.JTextArea commentArea;
	private javax.swing.JButton fichierBoutton;
	private javax.swing.JFileChooser choiseDeFichier;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JList listRequete;
	private javax.swing.JButton modificationBoutton;
	private javax.swing.JButton satisfaitBoutton;
	private javax.swing.JButton insatisfaitBoutton;
	private javax.swing.JButton quitterBoutton;
	private javax.swing.JTextArea requeteArea;
	private javax.swing.JLabel requetesJLabel;
	private javax.swing.JButton voirCommentaireBoutton;
	// End of variables declaration//GEN-END:variables
}
