package com.constantes;


public interface DatabaseConnectionConstantes {
	public final String USER_PROPS = "(prenom string, " + "nom string, " + "nomUtilisateur string, "
			+ "mdp string, " + "role string, " + "isAdministrateur string, " + "isDeleted string)";

	public final String BASE_USER_VALUES = ""
			+ "('Roger', 'Danfousse', 'rogerdanfousse', 'jesuiscool', 'client', 'false', 'false'),"
			+ "('Martine', 'Jodoin', 'martinejodoin', 'moiaussi', 'client', 'false', 'false'), "
			+ "('Pauline', 'St-Onge', 'popoline', 'monchatkiki', 'client', 'false', 'false'), "
			+ "('Richard', 'Chabot', 'chabotr', 'chabotte', 'technicien', 'false', 'false'), "
			+ "('Louise', 'Boisvert', 'techguest', 'password', 'technicien', 'false', 'false')";

	public final String REQUEST_PROPS = "(sujet string, " + "description string, "
			+ "nomUtilisateur string, " + "categorie string, " + "satisfaction string, "
			+ "fichier string, " + "administrateur string, " + "numero string, " + "statut string)";

	public final String COMMENTAIRE_PROPS = "(idNumeroRequete string, " + "auteur string, "
			+ "commentaire string)";

	public final String DATABASE_LOCATION = "jdbc:sqlite:dat/cranberry.db";
}
