package com.banquesFactices;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.singletonFactory.AbstractBanqueRequetes;

public class BanqueRequetesFactice extends AbstractBanqueRequetes {

	private static BanqueRequetesFactice instance = null;

	private BanqueRequetesFactice() throws FileNotFoundException, IOException {
		super();
	}

	public static BanqueRequetesFactice getInstance() throws FileNotFoundException, IOException {
		if (instance != null) {
			return instance;
		} else {
			instance = new BanqueRequetesFactice();
			instance.loadRequetes("fakeRequests", "fakeComments", "factice");
			return instance;
		}

	}
}
