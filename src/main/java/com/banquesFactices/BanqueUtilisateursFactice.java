package com.banquesFactices;

import java.io.FileNotFoundException;

import com.singletonFactory.AbstractBanqueUtilisateurs;

public class BanqueUtilisateursFactice extends AbstractBanqueUtilisateurs {
	private static BanqueUtilisateursFactice instance = null;

	private BanqueUtilisateursFactice() throws FileNotFoundException {
		super();
	}

	public static BanqueUtilisateursFactice getUserInstance() throws FileNotFoundException {
		if (instance != null) {
			return instance;
		} else {
			instance = new BanqueUtilisateursFactice();
			instance.loadUtilisateur("fakeUsers");
			return instance;
		}
	}
}
