package com.singletonFactory;

import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.constantes.AbstractBanqueUtilisateursConstantes;
import com.database.DatabaseConnection;
import com.objects.Client;
import com.objects.Technicien;
import com.objects.Utilisateur;

public abstract class AbstractBanqueUtilisateurs implements AbstractBanqueUtilisateursConstantes{

	protected ArrayList<Utilisateur> listeUtilisateurs;
	protected static DatabaseConnection databaseConnection = null;

	public AbstractBanqueUtilisateurs() {
		listeUtilisateurs = new ArrayList<Utilisateur>(100);
		databaseConnection = DatabaseConnection.getInstance();
	}

	public void save(String tableName) {
		databaseConnection.dropTable(tableName);
		databaseConnection.createUserTable(tableName, databaseConnection.USER_PROPS);
		databaseConnection.writeUsers(tableName, listeUtilisateurs);
	}

	public ArrayList getListeUtilisateursSelonRole(String role) {
		ArrayList<Utilisateur> listeUtilisateursRole = new ArrayList<Utilisateur>(100);
		for (int i = 0; i < listeUtilisateurs.size(); i++) {
			if (listeUtilisateurs.get(i).getRole().equals(role)) {
				listeUtilisateursRole.add(listeUtilisateurs.get(i));
			}
		}
		return listeUtilisateursRole;
	}

	public Utilisateur chercherUtilisateur(Utilisateur utilisateurCherchee) {
		for (Utilisateur user : listeUtilisateurs) {
			if (user.getNomUtilisateur().equals(utilisateurCherchee.getNomUtilisateur())) {
				return user;
			}
		}
		return new Client("utilisateurNonTrouvé", "utilisateurNonTrouvé", "utilisateurNonTrouvé",
				"utilisateurNonTrouvé", "utilisateurNonTrouvé", false, false);
	}

	public Utilisateur chercherUtilisateurParNomUtil(String nomVoulu) {
		for (Utilisateur utilisateur : listeUtilisateurs) {
			if (utilisateur.getNomUtilisateur().equalsIgnoreCase(nomVoulu)) {
				return utilisateur;
			}
		}
		return new Client("utilisateurNonTrouvé", "utilisateurNonTrouvé", "utilisateurNonTrouvé",
				"utilisateurNonTrouvé", "utilisateurNonTrouvé", false, false);
	}

	public void addUtilisateur(Utilisateur utilisateurAjouter) {
		boolean found = false;
		for (Utilisateur utilisateur : listeUtilisateurs) {
			if (utilisateur.getNomUtilisateur().equals(utilisateurAjouter.getNomUtilisateur())) {
				found = true;
				break;
			}
		}
		if (!found) {
			listeUtilisateurs.add(utilisateurAjouter);
		}
	}

	public void removeUtilisateur(Utilisateur utilisateurRemove) {
		int index = 0;
		boolean found = false;
		for (Utilisateur utilisateur : listeUtilisateurs) {
			if (utilisateur.getNomUtilisateur().equals(utilisateurRemove.getNomUtilisateur())) {
				found = true;
				break;
			}
			index++;
		}
		if (found) {
			listeUtilisateurs.remove(index);
		}
	}

	public void loadUtilisateur(String tableName) throws FileNotFoundException {
		databaseConnection = DatabaseConnection.getInstance();
		ResultSet rs = databaseConnection.select(tableName);
		ArrayList<String> proprieteUtilisateurs = null;
		if (rs != null) {
			try {
				while (rs.next()) {
					int i = 1;
					proprieteUtilisateurs = new ArrayList<String>();
					while (i <= NUMBER_OF_COLUMNS_USERS) {
						proprieteUtilisateurs.add(rs.getString(i++));
					}
					if (rs.getString("role").equals("client")) {
						this.addUtilisateur(new Client(proprieteUtilisateurs));
					} else {
						this.addUtilisateur(new Technicien(proprieteUtilisateurs));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
