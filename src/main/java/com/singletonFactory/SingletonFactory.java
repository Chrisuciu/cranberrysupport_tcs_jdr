package com.singletonFactory;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.banquesFactices.BanqueRequetesFactice;
import com.banquesFactices.BanqueUtilisateursFactice;
import com.constantes.SingletoneFactoryConstantes;
import com.objects.BanqueRequetes;
import com.objects.BanqueUtilisateurs;

public class SingletonFactory implements SingletoneFactoryConstantes {
	private static BanqueRequetes requetesReel;
	private static BanqueRequetesFactice requetesFactice;
	private static BanqueUtilisateurs utilisateursReel;
	private static BanqueUtilisateursFactice utilisateursFactice;

	public static AbstractBanqueUtilisateurs getAbstractBanqueUtilisateur(String type) {
		if (type == null) {
		} else if (type.equalsIgnoreCase(SINGLETON_REEL)) {
			try {
				utilisateursReel = BanqueUtilisateurs.getInstance();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			return utilisateursReel;
		}
		try {
			utilisateursFactice = BanqueUtilisateursFactice.getUserInstance();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return utilisateursFactice;
	}

	public static AbstractBanqueRequetes getAbstractBanqueRequetes(String type) {
		if (type == null) {
		} else if (type.equalsIgnoreCase(SINGLETON_REEL)) {
			try {
				requetesReel = BanqueRequetes.getInstance();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return requetesReel;
		}
		try {
			requetesFactice = BanqueRequetesFactice.getInstance();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return requetesFactice;
	}
}
