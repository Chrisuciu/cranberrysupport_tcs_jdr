package com.singletonFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.constantes.AbstractBanqueRequetesConstantes;
import com.database.DatabaseConnection;
import com.enums.Statut;
import com.objects.Commentaire;
import com.objects.Requete;
import com.objects.Technicien;
import com.objects.Utilisateur;

public abstract class AbstractBanqueRequetes implements AbstractBanqueRequetesConstantes {
	protected ArrayList<Requete> listeRequetes;
	protected DatabaseConnection databaseConnection;
	protected Integer numero = -1;

	public AbstractBanqueRequetes() {
		databaseConnection = DatabaseConnection.getInstance();
		listeRequetes = new ArrayList<Requete>(100);
	}

	public void newRequete(Requete requete) throws FileNotFoundException, IOException {
		listeRequetes.add(requete);
		requete.getClient().addRequete(requete);
	}

	public ArrayList<Requete> getListRequetes(Statut statut) {
		ArrayList<Requete> listeRetour = new ArrayList<Requete>();
		for (int i = 0; i < listeRequetes.size(); i++) {
			if (listeRequetes.get(i).getStatut().equals(statut)) {
				listeRetour.add(listeRequetes.get(i));
			}
		}
		return listeRetour;
	}

	public void assignerRequete(Technicien technicien, Requete requete) {
		requete.setStatut(Statut.enTraitement);
		requete.setTech(technicien);
	}

	public Requete getDerniereRequete() {
		return listeRequetes.get(listeRequetes.size() - 1);
	}

	public Integer incrementeNumeroRequete() {
		return ++numero;
	}

	public ArrayList<Commentaire> loadComments(String numero, String commentTableName,
			String tableType) {
		databaseConnection = DatabaseConnection.getInstance();
		ResultSet rsCom = databaseConnection.select(commentTableName);
		ArrayList<String> proprieteCommentaires = null;
		ArrayList<Commentaire> commentaires = new ArrayList<Commentaire>();
		if (rsCom != null) {
			try {
				while (rsCom.next()) {
					int i = 1;
					proprieteCommentaires = new ArrayList<String>();
					while (i <= NUMBER_OF_COLUMNS_COMMENT) {
						proprieteCommentaires.add(rsCom.getString(i++));
					}
					if (proprieteCommentaires.get(0).equals(numero)) {
						Utilisateur user = SingletonFactory.getAbstractBanqueUtilisateur(tableType)
								.chercherUtilisateurParNomUtil(proprieteCommentaires.get(1));
						commentaires.add(new Commentaire(proprieteCommentaires.get(2), user));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return commentaires;
	}

	public void loadRequetes(String requestTableName, String commentTableName, String tableType)
			throws FileNotFoundException, IOException {
		databaseConnection = DatabaseConnection.getInstance();
		ResultSet rsReq = databaseConnection.select(requestTableName);
		ArrayList<String> proprieteRequetes = null;
		ArrayList<Commentaire> commentaires;
		if (rsReq != null) {
			try {
				while (rsReq.next()) {
					int i = 1;
					proprieteRequetes = new ArrayList<String>();
					while (i <= NUMBER_OF_COLUMNS_REQUETE) {
						proprieteRequetes.add(rsReq.getString(i++));
					}
					Requete req = new Requete(proprieteRequetes, tableType);
					commentaires = loadComments(req.getNumero() + "", commentTableName, tableType);
					req.setListCommentaire(commentaires);
					listeRequetes.add(req);
					req.getClient().addRequete(req);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void saveRequetes(String requestTableName, String commentTableName) throws IOException {
		databaseConnection.dropTable(requestTableName);
		databaseConnection.dropTable(commentTableName);
		databaseConnection.createUserTable(requestTableName, databaseConnection.REQUEST_PROPS);
		databaseConnection.createUserTable(commentTableName, databaseConnection.COMMENTAIRE_PROPS);
		databaseConnection.writeRequest(requestTableName, commentTableName, listeRequetes);
	}

	public void dropRequestTables(String requestTableName, String commentTableName) {
		databaseConnection.dropTable(requestTableName);
		databaseConnection.createRequestTables(requestTableName);
	}

	public void dropCommenttTables(String requestTableName, String commentTableName) {
		databaseConnection.dropTable(commentTableName);
		databaseConnection.createCommentTables(commentTableName);
	}
}
