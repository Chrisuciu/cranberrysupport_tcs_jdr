package com.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.banquesFactices.BanqueRequetesFactice;
import com.banquesFactices.BanqueUtilisateursFactice;
import com.objects.BanqueRequetes;
import com.objects.BanqueUtilisateurs;
import com.singletonFactory.AbstractBanqueRequetes;
import com.singletonFactory.AbstractBanqueUtilisateurs;
import com.singletonFactory.SingletonFactory;

public class SingletonFactoryTest {

	SingletonFactory singletonFactory;
	AbstractBanqueRequetes requetesReel01;
	BanqueRequetes requetesReel02;
	BanqueRequetesFactice requetesFactice01;
	BanqueRequetesFactice requetesFactice02;
	AbstractBanqueUtilisateurs utilisateursReel01;
	BanqueUtilisateurs utilisateursReel02;
	BanqueUtilisateursFactice utilisateursFactice01;
	BanqueUtilisateursFactice utilisateursFactice02;
	String nullString = null;

	@Test
	public void testGetAbstractUtilisateur() {
		utilisateursReel01 = (BanqueUtilisateurs) singletonFactory
				.getAbstractBanqueUtilisateur("reel");
		assertTrue(utilisateursReel01 != null);
	}

	@Test
	public void testGetAbstractUtilisateurAlreadyInstanciated() {
		utilisateursReel01 = (BanqueUtilisateurs) singletonFactory
				.getAbstractBanqueUtilisateur("reel");
		utilisateursReel02 = (BanqueUtilisateurs) singletonFactory
				.getAbstractBanqueUtilisateur("reel");
		assertTrue(utilisateursReel01 == utilisateursReel02);
	}

	@Test
	public void testGetAbstractUtilisateurFactice() {
		utilisateursFactice01 = (BanqueUtilisateursFactice) singletonFactory
				.getAbstractBanqueUtilisateur("factice");
		assertTrue(utilisateursFactice01 != null);
	}

	@Test
	public void testGetAbstractUtilisateurFacticeAlreadyInstanciated() {
		utilisateursFactice01 = (BanqueUtilisateursFactice) singletonFactory
				.getAbstractBanqueUtilisateur("factice");
		utilisateursFactice02 = (BanqueUtilisateursFactice) singletonFactory
				.getAbstractBanqueUtilisateur("factice");
		assertTrue(utilisateursFactice01 == utilisateursFactice02);
	}

	@Test
	public void testGetAbstractRequetes() {
		requetesReel01 = (BanqueRequetes) singletonFactory.getAbstractBanqueRequetes("reel");
		assertTrue(requetesReel01 != null);
	}

	@Test
	public void testGetAbstractRequetesAlreadyInstanciated() {
		requetesReel01 = (BanqueRequetes) singletonFactory.getAbstractBanqueRequetes("reel");
		requetesReel02 = (BanqueRequetes) singletonFactory.getAbstractBanqueRequetes("reel");
		assertTrue(requetesReel01 == requetesReel02);
	}

	@Test
	public void testGetAbstractRequetesFactice() {
		requetesFactice01 = (BanqueRequetesFactice) singletonFactory
				.getAbstractBanqueRequetes("factice");
		assertTrue(requetesFactice01 != null);
	}

	@Test
	public void testGetAbstractRequetesFacticeAlreadyInstanciated() {
		requetesFactice01 = (BanqueRequetesFactice) singletonFactory
				.getAbstractBanqueRequetes("factice");
		requetesFactice02 = (BanqueRequetesFactice) singletonFactory
				.getAbstractBanqueRequetes("factice");
		assertTrue(requetesFactice01 == requetesFactice02);
	}

	@Test
	public void testGetAbstractUtilisateurTypeIsInvalid() {
		utilisateursReel01 = singletonFactory.getAbstractBanqueUtilisateur("foobar");
		assertTrue(utilisateursReel01 != null);
	}

	@Test
	public void testGetAbstractUtilisateurTypeIsNull() {
		utilisateursReel01 = singletonFactory.getAbstractBanqueUtilisateur(nullString);
		assertTrue(utilisateursReel01 != null);
	}

	@Test
	public void testGetAbstractRequetesTypeIsInvalid() {
		requetesReel01 = singletonFactory.getAbstractBanqueRequetes("foobar");
		assertTrue(requetesReel01 != null);
	}

	@Test
	public void testGetAbstractRequetesTypeIsNull() {
		requetesReel01 = singletonFactory.getAbstractBanqueRequetes(nullString);
		assertTrue(requetesReel01 != null);
	}
}
