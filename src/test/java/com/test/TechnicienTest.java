package com.test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.enums.Statut;
import com.objects.Client;
import com.objects.Requete;
import com.banquesFactices.BanqueUtilisateursFactice;
import com.enums.Categorie;
import com.objects.Technicien;
import com.singletonFactory.AbstractBanqueUtilisateurs;
import com.singletonFactory.SingletonFactory;

public class TechnicienTest {

	Technicien smallTechnicien;
	Technicien technicien;
	Technicien administrateur;
	Requete requete;
	static Client client;
	final String UTILISATEUR_FACTICE_PATH = "dat/BanqueUtilisateurFactice.txt";

	@Before
	public void before() throws FileNotFoundException, IOException {
		smallTechnicien = new Technicien("Billy", "Bob", "BillyBob", "billybobby", "technicien",
				false, false);
		technicien = new Technicien("Richard", "Chabot", "chabotr", "chabotte", "technicien", false,
				false);
		administrateur = new Technicien("admin", "admin", "sysAdmin", "123", "administrateur", true,
				false);
		client = new Client("Kevin", "Rondeau", "KevinRondeau", "mdp", "client", false, false);
		requete = new Requete("sujet", "description", client, Categorie.autre);
		technicien.addRequeteAssignee(requete);
	}

	@AfterClass
	public static void after() {
		AbstractBanqueUtilisateurs abstractBanqueUtilisateurs = SingletonFactory
				.getAbstractBanqueUtilisateur("factice");
		abstractBanqueUtilisateurs.removeUtilisateur(client);
		abstractBanqueUtilisateurs.save("fakeUsers");
	}

	@Test
	public void ajouterRequeteAssigneeTest() {
		assertEquals(1, technicien.getListeRequetesTotale().size());
	}

	@Test
	public void ajoutListRequetesFiniesTest() {
		technicien.addRequetesFinies(requete);
		assertEquals(0, technicien.getListeRequetesEnCours().size());
	}

	@Test
	public void getListRequetesFiniesTest() {
		technicien.addRequeteAssignee(requete);
		technicien.addRequetesFinies(requete);
		assertEquals(1, technicien.getListeRequetesEnCours().size());
	}

	@Test
	public void ajoutRequeteTest() {
		technicien.addRequete(requete);
		assertEquals(2, technicien.getListeRequetesTotale().size());
	}

	@Test
	public void getListeRequetesTest() {
		assertEquals(1, technicien.getListeRequetesTotale().size());
	}

	@Test
	public void getListPersoTest() {
		technicien.addRequete(requete);
		assertEquals(1, technicien.getListePersonnelle().size());
	}

	@Test
	public void getListStatutOuvertTest() {
		assertEquals(1, technicien.getListeSelonStatut(Statut.ouvert).size());
	}

	@Test
	public void getRequeteParStatutTest() throws FileNotFoundException, IOException {
		Requete requete2 = new Requete("hajhsaj", "akskask", client, Categorie.autre);
		requete2.setStatut(Statut.enTraitement);
		technicien.addRequeteAssignee(requete2);
		Requete requete3 = new Requete("hajhsaj", "akskask", client, Categorie.autre);
		requete3.setStatut(Statut.finalAbandon);
		technicien.addRequeteAssignee(requete3);
		Requete requete4 = new Requete("hajhsaj", "akskask", client, Categorie.autre);
		requete4.setStatut(Statut.finalSucces);
		technicien.addRequeteAssignee(requete4);

		assertEquals("Statut ouvert: 1\n" + "Statut En traitement: 1\n" + "Statut succès: 1\n"
				+ "Statut abandonnée: 1\n", technicien.getRequeteParStatut());
	}

	@Test
	public void getRoleTest() {
		assertEquals("technicien", technicien.getRole());
	}

	@Test
	public void testIsAdministrateur() {
		assertFalse(technicien.isAdministrateur());
	}

	@Test
	public void testSetIsAdministrateur() {
		technicien.setAdministrateur(true);
		assertTrue(technicien.isAdministrateur());
	}

	@Test
	public void deleteUtilisateurNotAdminTest() throws FileNotFoundException {
		technicien.deleteUtilisateur(smallTechnicien.getNomUtilisateur(), UTILISATEUR_FACTICE_PATH);
		assertFalse(smallTechnicien.isSupprime());
	}

	@Test
	public void deleteUtilisateurTest() throws FileNotFoundException {
		technicien.setAdministrateur(true);
		technicien.deleteUtilisateur(smallTechnicien.getNomUtilisateur(), UTILISATEUR_FACTICE_PATH);
		assertFalse(smallTechnicien.isSupprime());
	}

	@Test
	public void testCreateUser() {
		administrateur.createUser(client, "factice");
		try {
			int montantClients = BanqueUtilisateursFactice.getUserInstance()
					.getListeUtilisateursSelonRole("client").size();
			assertTrue(montantClients >= 4);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDeleteUtilisateur() {
		try {
			administrateur.deleteUtilisateur(technicien.getNomUtilisateur(), "factice");
			AbstractBanqueUtilisateurs abstractBanqueUtilisateurs = SingletonFactory
					.getAbstractBanqueUtilisateur("factice");
			Technicien tech = (Technicien) abstractBanqueUtilisateurs
					.chercherUtilisateurParNomUtil(technicien.getNomUtilisateur());
			assertTrue(tech.isSupprime() == true);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
