package com.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import com.enums.Satisfaction;

public class EnumSatisfactionTest {

	Satisfaction satisfactionTest;

	@Before
	public void before() {
		satisfactionTest = Satisfaction.nonDecide;
	}

	@Test
	public void getValeurStringTest() {
		assertEquals("Utilisateur n'a pas décicé", satisfactionTest.getValeurString());
	}

	@Test
	public void insatisfaitFromStringTest() {
		assertEquals(Satisfaction.insatisfait, satisfactionTest.fromString("insatisfait"));
	}

	@Test
	public void satisfaitFromStringTest() {
		assertEquals(Satisfaction.satisfait, satisfactionTest.fromString("satisfait"));
	}

	@Test
	public void nonDecideFromStringTest() {
		assertEquals(Satisfaction.nonDecide, satisfactionTest.fromString("nonDecide"));
	}
}
