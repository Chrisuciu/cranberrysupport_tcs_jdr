package com.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.objects.Utilisateur;
import com.objects.Client;
import com.objects.Technicien;
import com.singletonFactory.AbstractBanqueUtilisateurs;
import com.singletonFactory.SingletonFactory;

public class BanqueUtilisateursTest {

	static AbstractBanqueUtilisateurs banqueUtilisateursFactice;
	AbstractBanqueUtilisateurs banqueUtilisateursFactice02;
	static Client client01;
	static Client client02;
	Client clientRoger;
	Client bidon01;
	Technicien newtechnicien;
	final int NOMBRE_UTILISATEURS = 5;
	int nombre_clients;
	final int NOMBRE_TECHNICIENS = 2;

	@Before
	public void before() {
		client01 = new Client("Jean", "Belliveau", "ogBeliveau", "ogog", "client", false, false);
		client02 = new Client("Nicolo", "Machiavelli", "original007", "4theQueen", "client", false,
				false);
		clientRoger = new Client("Roger", "Danfousse", "rogerdanfousse", "jesuiscool", "client",
				false, false);
		bidon01 = new Client("John", "Bidon", "JohnBidon", "123456Seven", "client", false, false);
		newtechnicien = new Technicien("Chris", "Suciu", "chrisuciu", "chrisuciu", "technicien",
				true, false);
		banqueUtilisateursFactice = SingletonFactory.getAbstractBanqueUtilisateur("Factice");
		banqueUtilisateursFactice02 = SingletonFactory.getAbstractBanqueUtilisateur("Factice");
		nombre_clients = banqueUtilisateursFactice.getListeUtilisateursSelonRole("client").size();
	}

	@AfterClass
	public static void cleanTest() {
		banqueUtilisateursFactice.removeUtilisateur(client01);
		banqueUtilisateursFactice.removeUtilisateur(client02);
		banqueUtilisateursFactice.save("fakeUsers");
	}

	@Test
	public void testGetListUtilisateurs() {
		ArrayList<Utilisateur> liste = banqueUtilisateursFactice
				.getListeUtilisateursSelonRole("client");
		assertTrue(liste.size() == nombre_clients);
	}

	@Test
	public void testChercherUtilisateur() {
		banqueUtilisateursFactice.addUtilisateur(client01);
		assertTrue(banqueUtilisateursFactice.chercherUtilisateur(client01).getNomUtilisateur()
				.equals(client01.getNomUtilisateur()));
	}

	@Test
	public void testChercherUtilisateurNotFound() {
		assertTrue(banqueUtilisateursFactice.chercherUtilisateur(bidon01).getNom()
				.equals("utilisateurNonTrouvé"));
	}

	@Test
	public void testChercherNomRole() {
		assertEquals(
				banqueUtilisateursFactice
						.chercherUtilisateurParNomUtil(clientRoger.getNomUtilisateur()).getNom(),
				clientRoger.getNom());
	}

	@Test
	public void testChercherNomRoleNotFound() {
		assertTrue(banqueUtilisateursFactice.chercherUtilisateurParNomUtil(bidon01.getNom())
				.getNom().equals("utilisateurNonTrouvé"));
	}

	@Test
	public void removeUtilisateurTest() {
		newtechnicien.setAdministrateur(true);
		newtechnicien.createUser(bidon01, "factice");
		int tailleBanque = banqueUtilisateursFactice.getListeUtilisateursSelonRole("client").size();
		banqueUtilisateursFactice.removeUtilisateur(bidon01);
		banqueUtilisateursFactice.save("fakeUsers");
		assertEquals(tailleBanque - 1,
				banqueUtilisateursFactice.getListeUtilisateursSelonRole("client").size());
	}

	@Test
	public void testAddUtilisateur() {
		int nbClient = banqueUtilisateursFactice.getListeUtilisateursSelonRole("client").size();
		banqueUtilisateursFactice.addUtilisateur(client02);
		assertTrue(banqueUtilisateursFactice.getListeUtilisateursSelonRole("client")
				.size() == nbClient + 1);
	}

	@Test
	public void testAddUtilisateurDuplique() {
		int nbClient = banqueUtilisateursFactice.getListeUtilisateursSelonRole("client").size();
		banqueUtilisateursFactice.addUtilisateur(client02);
		assertTrue(banqueUtilisateursFactice.getListeUtilisateursSelonRole("client")
				.size() == nbClient);
	}
}
