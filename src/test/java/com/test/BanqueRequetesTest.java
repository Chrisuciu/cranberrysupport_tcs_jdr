package com.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.enums.Statut;
import com.objects.BanqueUtilisateurs;
import com.objects.Client;
import com.objects.Requete;
import com.enums.Categorie;
import com.objects.Technicien;
import com.singletonFactory.AbstractBanqueRequetes;
import com.singletonFactory.SingletonFactory;

@RunWith(MockitoJUnitRunner.class)
public class BanqueRequetesTest {

	AbstractBanqueRequetes banqueRequetes;
	Technicien technicien;
	Client client;
	@Mock
	Requete requete;
	@Mock
	BanqueUtilisateurs banqueUtilisateurs;

	@Before
	public void before() throws FileNotFoundException, IOException {
		
		banqueRequetes = SingletonFactory.getAbstractBanqueRequetes("factice");
		banqueRequetes.dropCommenttTables("fakeRequests", "fakeComments");
		technicien = new Technicien("Richard", "Chabot", "chabotr", "chabotte", "technicien", false,
				false);
		client = new Client("Roger", "Danfousse", "rogerdanfousse", "jesuiscool", "client", false,
				false);
		Mockito.when(requete.getStatut()).thenReturn(Statut.enTraitement);
		Mockito.when(requete.getTech()).thenReturn(technicien);
		Mockito.when(requete.getSujet()).thenReturn("test");
		Mockito.when(requete.getDescription()).thenReturn("test");
		Mockito.when(requete.getClient()).thenReturn(client);
		Mockito.when(requete.getCategorie()).thenReturn(Categorie.autre);
		Mockito.when(banqueUtilisateurs.chercherUtilisateurParNomUtil("Roger")).thenReturn(client);
	}

	@Test
	public void requeteAvecCommentaire() throws FileNotFoundException, IOException {
		Requete requeteCommentaire = new Requete("sujet", "description", client, Categorie.autre);
		banqueRequetes.newRequete(requeteCommentaire);
		banqueRequetes.getDerniereRequete().addCommentaire("commentaire", client);
		banqueRequetes.getDerniereRequete().addCommentaire("autre commentaire", client);
		banqueRequetes.saveRequetes("fakeRequests", "fakeComments");
		assertEquals(banqueRequetes.getDerniereRequete().getListeCommentaires().get(0).getComment(),
				"commentaire");
	}

	@Test
	public void newRequete() throws FileNotFoundException, IOException {
		banqueRequetes.newRequete(requete);
		Requete requeteLast = banqueRequetes.getDerniereRequete();
		boolean flag = false;
		if ("test".equals(requeteLast.getSujet()) && "test".equals(requeteLast.getDescription())
				&& client.equals(requeteLast.getClient())
				&& Categorie.autre.equals(requeteLast.getCategorie())) {
			flag = true;
		}
		assertTrue(flag);
	}

	@Test
	public void getListRequetes() throws FileNotFoundException, IOException {
		int count = banqueRequetes.getListRequetes(Statut.enTraitement).size();
		banqueRequetes.newRequete(requete);
		assertEquals(count + 1, banqueRequetes.getListRequetes(Statut.enTraitement).size());
	}

	@Test
	public void assignerRequete() {
		banqueRequetes.assignerRequete(technicien, requete);
		assertTrue(requete.getStatut().equals(Statut.enTraitement)
				&& requete.getTech().equals(technicien));
	}

	@Test
	public void returnLast() throws FileNotFoundException, IOException {
		banqueRequetes.newRequete(requete);
		Requete requeteLast = banqueRequetes.getDerniereRequete();
		boolean flag = false;
		if (requete.getSujet().equals(requeteLast.getSujet())
				&& requete.getDescription().equals(requeteLast.getDescription())
				&& requete.getClient().equals(requeteLast.getClient())
				&& requete.getCategorie().equals(requeteLast.getCategorie())) {
			flag = true;
		}
		assertTrue(flag);
	}

	@Test
	public void incrementeNo() {
		assertEquals(banqueRequetes.incrementeNumeroRequete().intValue() + 1,
				banqueRequetes.incrementeNumeroRequete().intValue());
	}
}
