package com.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.enums.Categorie;
import com.enums.Satisfaction;
import com.enums.Statut;
import com.objects.Client;
import com.objects.Requete;
import com.objects.Technicien;
import com.objects.Utilisateur;

public class RequeteTest {

	Requete req01;
	Requete req02;
	String sujet;
	String newSujet;
	String newDescription;
	String newCommentaire;
	String description;
	String filePath;
	String newFilePath;
	Client client01;
	Technicien tech01;
	Utilisateur client02;

	@Before
	public void before() {
		sujet = "requete01";
		newSujet = "newRequeteSujet";
		description = "Ceci est un test";
		newDescription = "Ceci est une nouvelle description";
		newCommentaire = "Ceci est un commentaire";
		filePath = "dat\\BanqueRequetes.txt";
		newFilePath = "dat\\BanqueRequetesFactice.txt";
		client01 = new Client("Cristian", "Suciu", "chrisuciu", "123", "client", false, false);
		tech01 = new Technicien("Jean-Daniel", "Rondeau", "JeDaRo", "123", "technicien", false,
				false);
		client02 = new Client("Jean-Daniel", "Rondeau", "JERADO", "456", "client", false, false);
		try {
			req01 = new Requete(sujet, description, client01, Categorie.posteDeTravail);
			req02 = new Requete(sujet, description, client02, Categorie.serviceWeb,
					Satisfaction.nonDecide);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetSujet() {
		assertTrue(req01.getSujet().equals(sujet));
	}

	@Test
	public void testSetSujet() {
		req01.setSujet(newSujet);
		assertTrue(req01.getSujet().equals(newSujet));
	}

	@Test
	public void testSetDescription() {
		req01.setDescription(newDescription);
		assertTrue(req01.getDescription().equals(newDescription));
	}

	@Test
	public void testUploadFichier() {
		req01.setFichier(newFilePath);
		assertTrue(req01.getFichier().getPath().equals(newFilePath));
	}

	@Test
	public void testSetCategorie() {
		req01.setCategorie(Categorie.serviceWeb);
		assertTrue(req01.getCategorie() == Categorie.serviceWeb);
	}

	@Test
	public void testGetFichierNullFile() {
		assertTrue(req01.getFichier() == null);
	}

	@Test
	public void testSetStatut() {
		req01.setStatut(Statut.ouvert);
		assertTrue(req01.getStatut() == Statut.ouvert);
	}

	@Test
	public void testFinaliser() {
		req01.setTech(tech01);
		req01.finaliser(Statut.finalSucces);
		assertTrue(req01.getStatut() == Statut.finalSucces);
	}

	@Test
	public void testAddCommentaire() {
		req01.addCommentaire(newCommentaire, client01);
		assertTrue(req01.getListeCommentaires().size() == 1);
	}

	@Test
	public void testSetTech() {
		req01.setTech(tech01);
		assertTrue(req01.getTech().getNom().equals(tech01.getNom()));
	}

	@Test
	public void testSetFile() {
		req01.setFile(new File(filePath));
		assertEquals(req01.getFichier().getPath(), filePath);
	}

	@Test
	public void testSetFileWhenNull() {
		assertTrue(req01.getFichier() == null);
	}

	@Test
	public void testGetNumero() {
		assertTrue(req01.getNumero() instanceof Integer);
	}

	@Test
	public void testGetStatut() {
		req01.setStatut(Statut.enTraitement);
		assertTrue(req01.getStatut() == Statut.enTraitement);
	}

	@Test
	public void testGetComments() {
		assertTrue(req01.getListeCommentaires().size() == 0);
	}

	@Test
	public void testGetCategorie() {
		req01.setCategorie(Categorie.serviceWeb);
		assertTrue(req01.getCategorie() == Categorie.serviceWeb);
	}

	@Test
	public void testGetTech() {
		req01.setTech(tech01);
		assertTrue(req01.getTech().getNomUtilisateur().equals(tech01.getNomUtilisateur()));
	}

	@Test
	public void testGetTechWhenNull() {
		assertTrue(req01.getTech() == null);
	}

	@Test
	public void testGetClient() {
		assertTrue(req01.getClient().getNomUtilisateur().equals(client01.getNomUtilisateur()));
	}

	@Test
	public void testGetDescrip() {
		assertTrue(req01.getDescription().equals(description));
	}

	@Test
	public void getSatisfactionTest() {
		assertEquals(req01.getSatisfaction(), Satisfaction.nonDecide);
	}

	@Test
	public void setSatisfactionTest() {
		Satisfaction satisfaction = req01.getSatisfaction();
		req01.setSatisfaction(Satisfaction.satisfait);
		assertNotEquals(req01.getStatut(), satisfaction);
	}
}
