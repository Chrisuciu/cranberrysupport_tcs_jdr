package com.test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.enums.Categorie;
import com.enums.Statut;
import com.objects.Client;
import com.objects.Requete;

public class ClientTest {

	Client client01;
	Requete req01;
	String nom;
	String prenom;
	String nomUtil;
	String password;
	String role;

	@Before
	public void before() {
		nom = "Roger";
		prenom = "Danfousse";
		nomUtil = "rogerdanfousse";
		password = "jesuiscool";
		role = "client";
		client01 = new Client(nom, prenom, nomUtil, password, role, false, false);
		try {
			req01 = new Requete("Exemple 1", "Test d'une requete", client01,
					Categorie.posteDeTravail);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetRole() {
		assertTrue(client01.getRole().equals(role));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testGetListStatut() {
		client01.getListeSelonStatut(Statut.ouvert);
	}

	@Test
	public void testAjoutRequete() {
		client01.addRequete(req01);
		assertTrue(client01.getListeRequetesTotale().size() == 1);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testGetListPerso() {
		client01.getListePersonnelle();
	}

	@Test
	public void testTrouverRequete() throws FileNotFoundException, IOException {
		client01.addRequete(req01);
		assertTrue(client01.trouverRequeteParSujet(req01.getSujet()).getSujet()
				.equals(req01.getSujet()));
	}

	@Test
	public void testTrouverRequeteNotFound() throws FileNotFoundException, IOException {
		assertTrue(client01.trouverRequeteParSujet(req01.getSujet()).getSujet()
				.equals("requeteNonTrouvé"));
	}

	@Test
	public void testGetListeRequetes() {
		assertTrue(client01.getListeRequetesTotale().isEmpty() == true);
	}

}
