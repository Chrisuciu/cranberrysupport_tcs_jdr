package com.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.objects.Client;
import com.objects.Technicien;
import com.objects.Utilisateur;

public class UtilisateurTest {

	Utilisateur client;
	Technicien technicien;
	String prenom = "prenom";
	String nom = "nom";
	String nomUtilisateur = "prenomNom";
	String mdp = "mdp";
	String newMdp = "newMdp";
	String roleClient = "client";
	String roleTech = "technicien";
	String chaineBidon = "asdasd";
	String newPrenom = "newPrenom";
	String newNom = "newNom";
	boolean isSupprime = false;
	boolean isAdmin = false;

	@Before
	public void before() {
		client = new Client(prenom, nom, nomUtilisateur, mdp, roleClient, isAdmin, isSupprime);
		technicien = new Technicien(prenom, nom, nomUtilisateur, mdp, roleTech, isAdmin,
				isSupprime);
	}

	@Test
	public void fetchInfoTest() {
		assertTrue(technicien.fetchInfos().get(0).equals(nom)
				&& technicien.fetchInfos().get(1).equals(prenom)
				&& technicien.fetchInfos().get(2).equals(nomUtilisateur)
				&& technicien.fetchInfos().get(3).equals(roleTech));
	}

	@Test
	public void getNomTest() {
		assertEquals(nom, client.getNom());
	}

	@Test
	public void getNomUtilTest() {
		assertEquals(nomUtilisateur, technicien.getNomUtilisateur());
	}

	@Test
	public void getRoleTest() {
		assertEquals(roleTech, technicien.getRole());
	}

	@Test
	public void loginTest() {
		assertTrue(technicien.verificationConnexion(nomUtilisateur, mdp));
	}

	@Test
	public void loginFalseTest() {
		assertFalse(technicien.verificationConnexion(chaineBidon, mdp));
	}

	@Test
	public void testGetPrenom() {
		assertTrue(client.getPrenom().equals(prenom));
	}

	@Test
	public void testSetPrenom() {
		client.setPrenom(newPrenom);
		assertTrue(client.getPrenom().equals(newPrenom));
	}

	@Test
	public void testIsSupprime() {
		assertTrue(client.isSupprime() == isSupprime);
	}

	@Test
	public void testSetSupprime() {
		client.setSupprime(!isSupprime);
		assertTrue(client.isSupprime() == !isSupprime);
	}

	@Test
	public void testGetMdp() {
		assertTrue(client.getMotDePasse().equals(mdp));
	}

	@Test
	public void testSetMdp() {
		client.setMotDePasse(newMdp);
		assertTrue(client.getMotDePasse().equals(newMdp));
	}

	@Test
	public void getIsAdministrateur() {
		assertFalse(technicien.isAdministrateur());
	}

	@Test
	public void setIsAdministrateur() {
		technicien.setAdministrateur(true);
		assertTrue(technicien.isAdministrateur());
	}
}
