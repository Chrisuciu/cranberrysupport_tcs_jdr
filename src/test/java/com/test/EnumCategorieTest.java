package com.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.enums.Categorie;

public class EnumCategorieTest {

	String posteDeTravailEnum = "Poste de travail";
	String serveurEnum = "Serveur";
	String serviceWebEnum = "Service web";
	String compteUsagerEnum = "Compte usager";
	String autreEnum = "Autre";
	String nullEnum = "Bidon";

	@Test
	public void testFromStringPosteDeTravail() {
		assertTrue(Categorie.fromString(posteDeTravailEnum) == Categorie.posteDeTravail);
	}

	@Test
	public void testFromStringServeur() {
		assertTrue(Categorie.fromString(serveurEnum) == Categorie.serveur);
	}

	@Test
	public void testFromStringServiceWeb() {
		assertTrue(Categorie.fromString(serviceWebEnum) == Categorie.serviceWeb);
	}

	@Test
	public void testFromStringCompteUsager() {
		assertTrue(Categorie.fromString(compteUsagerEnum) == Categorie.compteUsager);
	}

	@Test
	public void testFromStringAutreEnum() {
		assertTrue(Categorie.fromString(autreEnum) == Categorie.autre);
	}

	@Test
	public void testFromStringNullEnum() {
		assertTrue(Categorie.fromString(nullEnum) == Categorie.autre);
	}

}
