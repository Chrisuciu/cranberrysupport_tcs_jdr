package com.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.enums.Statut;

public class EnumStatutTest {
	String ouvertEnum = "ouvert";
	String enTraitementEnum = "en traitement";
	String finalAbandonEnum = "Abandon";
	String finalSuccesEnum = "Succès";
	String bidon = "Bidon";

	@Test
	public void testFromStringOuvert() {
		assertTrue(Statut.fromString(ouvertEnum) == Statut.ouvert);
	}

	@Test
	public void testFromStringEnTraitement() {
		assertTrue(Statut.fromString(enTraitementEnum) == Statut.enTraitement);
	}

	@Test
	public void testFromStringfinalAbandon() {
		assertTrue(Statut.fromString(finalAbandonEnum) == Statut.finalAbandon);
	}

	@Test
	public void testFromStringfinalSucces() {
		assertTrue(Statut.fromString(finalSuccesEnum) == Statut.finalSucces);
	}

	@Test
	public void testFromStringNull() {
		assertTrue(Statut.fromString(bidon) == Statut.ouvert);
	}

}
