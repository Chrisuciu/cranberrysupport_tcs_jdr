package com.test;

import org.junit.Before;
import org.junit.Test;

import com.objects.Client;
import com.objects.Commentaire;
import static org.junit.Assert.*;

public class CommentaireTest {

	Commentaire commentaire;
	Client client;

	@Before
	public void before() {
		client = new Client("prenom", "nom", "prenomnom", "mdp", "client", false, false);
		commentaire = new Commentaire("test", client);
	}

	@Test
	public void getAuteurTest() {
		assertEquals(client, commentaire.getAuteur());
	}

	@Test
	public void getCommentTest() {
		assertEquals("test", commentaire.getComment());
	}

	@Test
	public void toStringTest() {
		assertEquals("prenomnom: test\n", commentaire.toString());
	}
}
